﻿using System;

namespace dotnetcore
{

    // Adaptee: The existing class "Apple" with its own methods.
    public class Apple
    {
        public void Peel()
        {
            Console.WriteLine("Peeling the apple.");
        }

        public void Eat()
        {
            Console.WriteLine("Eating the apple.");
        }
    }

    // Target: The new interface "IFruit" that we want to adapt "Apple" to.
    public interface IFruit
    {
        void Prepare();
        void Consume();
    }

    // Adapter: Adapting "Apple" to the "IFruit" interface.
    public class AppleAdapter : IFruit
    {
        private readonly Apple _apple;

        public AppleAdapter(Apple apple)
        {
            _apple = apple;
        }

        public void Prepare()
        {
            _apple.Peel();
        }

        public void Consume()
        {
            _apple.Eat();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Apple apple = new Apple();
            IFruit adaptedApple = new AppleAdapter(apple);

            Console.WriteLine("Preparing and consuming the adapted fruit:");
            adaptedApple.Prepare();
            adaptedApple.Consume();
        }
    }
}
